// 
// db.fruits.insertMany([
//     {
//         "name": "Apple",
//         "supplier": "Red Farms Inc.",
//         "stocks": 20,
//         "price": 40,
//         "onSale": true
//     },
//     {
// 
//         "name": "Banana",
//         "supplier": "Yellow Farms",
//         "stocks": 15,
//         "price": 20,
//         "onSale": true
//     },
//     {
//         "name": "Kiwi",
//         "supplier": "Green Farming and Canning",
//         "stocks": 25,
//         "price": 50,
//         "onSale": true
//     },
//     {
//         "name": "Mango",
//         "supplier": "Yellow Farms",
//         "stocks": 10,
//         "price": 60,
//         "onSale": true
//     },
//     {
//         "name": "Dragon Fruits",
//         "supplier": "Red Farms Inc.",
//         "stocks": 10,
//         "price": 60,
//         "onSale": true
//     },
// ])

/*
	Aggregation Pipeline Stages

	Aggregation is typically done in 2-3 steps. Each process in aggregation is called a stage.

	$match - is used to match or get documents which satisfy the condition
	//syntax: {$match: {field:<value>}}

	$group - allows us to group together documents and create an analysis out of the grouped documents.

	_id: in the group stage, essentially associares an id to our results.
	_id: also determines the number of groups.

	_id: "$supplier" - essentially group together documents with the same values in the supplier field
	id: $<field> groups the documents in the given field

*/

db.fruits.aggregate([
    
    // looked for and got all fruits that are onSale.
    {$match: {onSale:true}},
    
    /*
		apple
		supplier: Red Farms Inc

		banana
		supplier: Yellow Farms

		mango
		supplier: Yellow Farms

		kiwi
		supplier: Green Farming

		dragon fruit
		supplier: Red Farms Inc


		group1
		_id: Red Farms Inc
		apple 
		dragon fruit

		group2
		_id: Yellow Farms
		banana
		mango

		group3
		_id: Green Farming
		kiwi

		$sum is used to add or total the values of the given field

		group1
		apple
		stocks: 20
		dragon fruit
		stocks: 10
		$sum:30


		group2
		banana
		stocks: 15
		mango
		stocks: 10
		$sum:25

		group3
		kiwi
		stocks: 25
		$sum:25
    */
    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}

])
    
db.fruits.aggregate([
        
        // If the _id's value is definite or given, $group will only create one group.
	{$match: {onSale:true}},
	{$group: {_id:null,totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

        // If the _id's value is definite or given, $group will only create one group.
	{$match: {onSale:true}},
	{$group: {_id:"AllOnSaleFruits",totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

        // If the _id's value is definite or given, $group will only create one group.
	{$match: {supplier:"Red Farms Inc"}},
	{$group: {_id:"RedFarmsInc",totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

        //get the total stocks of all products onSale supplied by Yellow Farms 
        //{$match: {$and:[{supplier:"Yellow Farms"}, {onSale:true}]}},
        
	{$match: {onSale:true, supplier:"Yellow Farms"}},
	{$group: {_id:"Yellow Farms",totalStocks:{$sum:"$stocks"}}}

])
// 
// //$avg - is an operator used in the $group stage.
// // $avg gets the avg of the numurical values of the indicated field in grouped documents
// 
// db.fruits.aggregate([
//     {$match:(onSale:true}},
//     {$group:{_id:"$supplier"}, avgStock: {$avg:"$stocks"}}}
//     
// ]) 
        
db.fruits.aggregate([
        {$match: {onSale:true}},
        {$group: {_id:null, avgPrice:{$avg:"$price"}}}
])

// $max - will allow us to get the highest value out of all the values in a given field per group

// highest number of stock for all items on sale.
db.fruits.aggregate([

		{$match:{onSale:true}},
		{$group:{_id:"highestStockOnSale", maxStock:{$max:"$stocks"}}}
])

// highest price for all items on sale
db.fruits.aggregate([

		{$match:{onSale:true}},
		{$group:{_id:null, maxPrice:{$max:"$price"}}}
])

// $min - gets the lowest value of the values in a given field per group.

// get the lowest number of stocks for all items on sale
db.fruits.aggregate([

		{$match:{onSale:true}},
		{$group:{_id:"lowestStockOnSale", minStock:{$min:"$stocks"}}}

])

// lowest price for all items on sale:
db.fruits.aggregate([

		{$match:{onSale:true}},
		{$group:{_id:"lowestPriceOnSale", minPrice:{$min:"$price"}}}
])

/*
	Mini Activity
*/


db.fruits.aggregate([

		{$match:{price: {$lt:50}}},
		{$group:{_id:"lowestStock", minStock:{$min:"$stocks"}}}

])

// Other Stages
// $count - is a stage added after $match stage to count all items that matches our criteria.


// count all items on sale
db.fruits.aggregate([
                {$match:{onSale:true}},
		{$count:"itemsOnSale"}

])

// count all items whose price is less than 50
db.fruits.aggregate([

		{$match:{price:{$lt:50}}},
		{$count:"itemsPriceLessThan50"}

])

// number of items with stocks less than 20
db.fruits.aggregate([

		{$match:{stocks:{$lt:20}}},
		{$count:"forRestock"}

])

// $out - save/output results in a new collection
// note: This will overwrite the collection if it already exists

db.fruits.aggregate([



		{$match:{onSale:true}},
		{$group:{_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
		{$out:"stocksPerSupplier"}



])