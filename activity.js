// 1.
db.fruits.aggregate([

		{$match:{supplier:"Yellow Farms", price:{$lt:50}}},
		{$count:"totalNumberOfItems"}
])


// 2. 
db.fruits.aggregate([

		{$match:{price:{$lt:30}}},
		{$count:"itemsPriceLessThan30"}
])

// 3.

db.fruits.aggregate([

		{$match:{supplier:"Yellow Farms"}},
		{$group: {_id:"itemsAveragePrice", avgPrice:{$avg:"$price"}}}
])

// 4.

db.fruits.aggregate([

		{$match:{supplier:"Red Farms Inc."}},
		{$group:{_id:"highestPrice", maxPrice:{$max:"$price"}}}
])

// 5.

db.fruits.aggregate([
                
		{$match:{supplier:"Red Farms Inc."}},
		{$group:{_id:"lowestPrice", minPrice:{$min:"$price"}}}
])